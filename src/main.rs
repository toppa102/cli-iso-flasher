use clap::Parser;
use sha2::{Digest, Sha256};
use windows::imp::GetLastError;
use std::io::Read;
use std::fs::File;
use std::path::Path;
use windows::core::PCSTR;
use windows::Win32::Foundation::{HANDLE, CloseHandle};
use windows::Win32::Storage::FileSystem::*;
use windows::Win32::System::Ioctl::FSCTL_DISMOUNT_VOLUME;
use windows::Win32::System::IO::{DeviceIoControl, OVERLAPPED, OVERLAPPED_0, OVERLAPPED_0_0};

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    iso_path_or_url: String,
    drive: u32,
}

fn write_iso<T: Read>(handle: HANDLE, reader: &mut T) -> Vec<u8> {
    let mut bytes_written: usize = 0;
    let mut accumulator = Vec::with_capacity(1024 * 1024);

    let mut hasher = Sha256::new();

    loop {
        let mut data = [0u8; 4096];
        let bytes_read = reader.read(&mut data).unwrap();
        hasher.update(&data[..bytes_read]);

        accumulator.extend_from_slice(&data[..bytes_read]);

        if accumulator.len() > 1024 * 1024 {
            let offset = bytes_written.to_le_bytes();

            let mut overlap = OVERLAPPED {
                Anonymous: OVERLAPPED_0 {
                    Anonymous: OVERLAPPED_0_0 {
                        Offset: u32::from_le_bytes(offset[0..4].try_into().unwrap()),
                        OffsetHigh: u32::from_le_bytes(offset[4..8].try_into().unwrap()),
                    },
                },
                ..Default::default()
            };

            let mut size = 1024 * 1024;
            let is_ok = unsafe {
                WriteFile(
                    handle,
                    Some(&mut accumulator[..1024 * 1024]),
                    Some(&mut size),
                    Some(&mut overlap),
                )
            };

            if is_ok.0 == 0 {
                println!("Error : {}", unsafe{GetLastError()});
            }

            bytes_written += 1024 * 1024;
            accumulator.drain(..1024 * 1024);
        }
        if bytes_read == 0 {
            let offset = bytes_written.to_le_bytes();

            let mut overlap = OVERLAPPED {
                Anonymous: OVERLAPPED_0 {
                    Anonymous: OVERLAPPED_0_0 {
                        Offset: u32::from_le_bytes(offset[0..4].try_into().unwrap()),
                        OffsetHigh: u32::from_le_bytes(offset[4..8].try_into().unwrap()),
                    },
                },
                ..Default::default()
            };

            let mut size = (accumulator.len() as u32 / 4096 + 1) * 4096;
            accumulator.resize(size as usize, 0);

            let is_ok = unsafe {
                WriteFile(
                    handle,
                    Some(&mut accumulator[..size as usize]),
                    Some(&mut size),
                    Some(&mut overlap),
                )
            };

            if is_ok.0 == 0 {
                println!("Error : {}", unsafe{GetLastError()});
            }

            break;
        }
    }

    unsafe { FlushFileBuffers(handle) };

    hasher.finalize().to_vec()
}

fn main() {
    let cli = Cli::parse();
    let handle = unsafe {
        CreateFileA(
            PCSTR::from_raw(format!("\\\\.\\PhysicalDrive{}", cli.drive).as_ptr()),
            FILE_GENERIC_WRITE.0 | FILE_GENERIC_READ.0,
            FILE_SHARE_READ | FILE_SHARE_WRITE,
            None,
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL,
            None,
        )
    }
    .unwrap();

    unsafe {
        DeviceIoControl(handle, FSCTL_DISMOUNT_VOLUME, None, 0, None, 0, None, None);
    }

    let hash = {
        if &cli.iso_path_or_url[..4] == "http" {
            let url = cli.iso_path_or_url;
            let res = ureq::get(&url).call().unwrap();
            write_iso(handle, &mut res.into_reader())
        } else {
            let path = Path::new(&cli.iso_path_or_url);
            let mut file = File::open(path).unwrap();
            write_iso(handle, &mut file)
        }
    };

    for n in hash {
        print!("{:02x}", n);
    }
    println!();

    unsafe { CloseHandle(handle); }
}
